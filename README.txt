Note that the pendant series is still a WIP and subject to changes before release.

Anwaar are families of standardized boards for making your own personal pcb token. This folder contains the KiCad design files as well as readble exports and misc. project files.

About this repository:
3D - 3d Exports of board (and pngs)
Drawings - Folder for SVG or DXF exports
License - License information
PDF - PDF export of schmatic
Production Files - Files for fabrication

changes.txt - As per CERN OHL V2 changes made to this must be listed with a times stamp in this file
